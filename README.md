# postscriptum-pkg

This project contains the build scripts to package the Postscriptum application.

It is also a main component of the development environment. Please consult the project `[postscriptum-wsp](https://gitlab.com/postscriptum.app/wsp)` for more information.

## Postscriptum

Postscriptum is a solution to produce PDF from HTML with the help of CSS Print.

Please refer to [__https://postscriptum.app__](https://postscriptum.app) for a global picture of the project.

## Issues

Please report any issues on the [*postscriptum-core* project](https://gitlab.com/postscriptum.app/core/-/issues).
