const acceptedTypes = ["text/html", "application/xhtml+xml", "text/plain"];
let extUi: HTMLElement;
let processBtn: HTMLButtonElement;
if (!document.documentElement.hasAttribute("ps-ext-disabled") && acceptedTypes.includes(document.contentType)) {
	extUi = document.createElement('ps-ext-ui');
	const sr = extUi.attachShadow({mode: "open"});
	const cssLink = sr.appendChild(document.createElement('link'));
	cssLink.setAttribute('rel', 'stylesheet');
	cssLink.setAttribute('href', 'chrome-extension://iannnbdlkeogdclhfjkalnlmofpjdjhp/css/ui.css');
	processBtn = document.createElement("button") as HTMLButtonElement;
	processBtn.id = "process";
	processBtn.title = "Paginate the page with Postscriptum";
	processBtn.onclick = async () => {
		processBtn.disabled = true;
		await chrome.runtime.sendMessage({type: 'process'});
	};
	sr.appendChild(processBtn);

	const outputPdfBtn = document.createElement("button") as HTMLButtonElement;
	outputPdfBtn.id = "outputPdf";
	outputPdfBtn.title = "Convert the page to PDF";
	outputPdfBtn.onclick = () => chrome.runtime.sendMessage({type: 'outputPdf'});
	sr.appendChild(outputPdfBtn);

	chrome.runtime.onMessage.addListener(onMessage);

	document.documentElement.appendChild(extUi);
}

async function onMessage(msg: InMessage): Promise<void> {
	if (!extUi) return;
	const {type} = msg;
	if (type == "error") {
		const error = new Error(msg.message);
		error.stack = msg.stack;
		console.error(error);
		extUi.setAttribute('ps-status', 'error');
		processBtn.disabled = false;
	} else if (type == 'processed') {
		extUi.removeAttribute('ps-status');
		processBtn.disabled = false;
	}
}

