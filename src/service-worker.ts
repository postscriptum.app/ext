/* eslint-disable @typescript-eslint/consistent-type-imports */
import MessageSender = chrome.runtime.MessageSender;

type OutMessage = import("@postscriptum.app/cli/dist/messages").InMessage;
type InMessage = import("@postscriptum.app/cli/dist/messages").OutMessage;

let port: chrome.runtime.Port;
const processTabs: Set<number> = new Set();

let config: Config = {printPreview: true};

chrome.storage.local.get(config, (storageConfig: Config) => {
	config = storageConfig;
});

chrome.storage.onChanged.addListener(function (items) {
	for (const key in items) {
		config[key] = items[key].newValue;
	}
});


async function createNativePort(): Promise<chrome.runtime.Port> {
	const extInfo = await chrome.management.getSelf();
	const [majorVers, mediumVers] = extInfo.version.split('.');
	const version = `${majorVers}.${mediumVers}`;

	port = chrome.runtime.connectNative(`app.postscriptum.messages_${version}`);
	port.onDisconnect.addListener(async () => {
		console.error(new Error("The native messaging with the Postscriptum process has been interrupted."));
		port = null;
	});
	port.onMessage.addListener(onNativeMessage);
	return port;
}


async function targetFromTabId(tabId): Promise<chrome._debugger.TargetInfo> {
	const targets = await chrome.debugger.getTargets();
	for (const target of targets) {
		if (target.tabId == tabId) return target;
	}
	throw new Error(`Target for tab ${tabId} not found`);
}

async function postNativeMessage(msg: OutMessage): Promise<void> {
	console.log('-->', msg);
	if (!port) await postContentMessage(msg.tabId, {type: 'error', message: "The native messaging with the Postscriptum process has been interrupted."});
	else port.postMessage(msg);
}

async function onNativeMessage(msg: InMessage): Promise<void> {
	console.log('<--', msg);
	const {type, tabId} = msg;
	delete msg.tabId;
	if (type == "error") {
		const error = new Error(msg.message);
		error.stack = msg.stack;
		console.error(error);
		for (const tabId of processTabs) await postContentMessage(tabId, msg);
	} else if (type == "pdfOutput") {
		await chrome.tabs.create({url: msg.url});
	} else {
		delete msg.tabId;
		await postContentMessage(tabId, msg);
	}
}

async function postContentMessage(tabId: number, msg: any): Promise<void> {
	console.log('->', msg);
	await chrome.tabs.sendMessage(tabId, msg);
}

async function onContentMessage({type}: Pick<OutMessage, "type">, {tab}: MessageSender): Promise<void> {
	console.log('<-', {type});
	const {id: targetId} = await targetFromTabId(tab.id);
	const msg: OutMessage = {type, tabId: tab.id, targetId};
	if (msg.type == "process") {
		if (config.printPreview) msg.plugins = ['ps-print-preview'];
		processTabs.add(msg.tabId);
	}
	await postNativeMessage(msg);
}

createNativePort().then(() => chrome.runtime.onMessage.addListener(onContentMessage)).catch(console.error);
chrome.tabs.onRemoved.addListener((tabId) => processTabs.delete(tabId));

chrome.commands.onCommand.addListener(async (command, tab) => {
	await onContentMessage({type: command as 'process' | 'outputPdf'}, {tab});
});


chrome.commands.getAll().then((commands) => console.log(commands));
