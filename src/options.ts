interface Config {
	printPreview: boolean;
}

document.addEventListener('DOMContentLoaded', async () => {
	const printPreviewInput = document.getElementById('printPreview') as HTMLInputElement;
	let config: Config = {printPreview: true};

	chrome.storage.local.get(config, (storageConfig: Config) => {
		console.log("Initial config:", storageConfig);
		config = storageConfig;
		configUpdated();
	});

	chrome.storage.onChanged.addListener((items) => {
		for (const key in items) {
			config[key] = items[key].newValue;
		}
		configUpdated();
	});

	function configUpdated(): void {
		console.log("Config updated:", config);
		printPreviewInput.checked = config.printPreview;
	}

	printPreviewInput.onchange = () => {
		config.printPreview = printPreviewInput.checked;
		chrome.storage.local.set(config);
	};
});
