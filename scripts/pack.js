const fsp = require('node:fs/promises');
const os = require('node:os');
const crx3 = require('crx3');

(async () => {
	const baseDir = `${__dirname}/..`;
	let tmpDir;
	try {
		let keyPath;
		if (process.env.EXT_KEY) {
			tmpDir = await fsp.mkdtemp(`${os.tmpdir()}/crxKey-`);
			keyPath = `${tmpDir}/key.pem`;
			await fsp.writeFile(keyPath, process.env.EXT_KEY);
		} else {
			keyPath = `${baseDir}/key.pem`;
		}
		await crx3([ `${baseDir}/dist/manifest.json`], {
			keyPath,
			crxPath: `${baseDir}/postscriptum-ext.crx`
		})
	}
	finally {
		if (tmpDir) await fsp.rm(tmpDir, { recursive: true });
	}
})();
